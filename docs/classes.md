## Структура классов `app.py`

**app.py** - файл, включающий в себя основной функционал, построен на классах, описывающих сущности БД.
Среди них:
* Users
* Tokens
* Components
* Equipment
* Orders

Каждый из перечисленных классов наследуется от `db.Model`, что делает его сущностью **SQLAlchemy**.

Каждый из перечисленных классов включает в себя методы, реализующие все нехобходимые функциональные возможности: изменяет, удаляет, добавляет данные в БД, производит их обработку.
Методы классов не требуют отдельных описаний: методы, выполняющие неочевидные действия, задокументированы в коде.

Для удобства работы с кодом, при задании методов класса использовались следующие правила:
* Если функция опирается на данные формы - она имеет префикс `_from_form` в названии и/или включает в себя обязательный аргумент `form`.
* В названиях функций присутсвуют такие части, как `set`, `get`, `delete`, `add`, `check`, `save`, используемые в соответствии со стандартом написания python-кода **PEP8**
* Большинство функций возвращают целочисленные значения - **1** или **0**, означающие успех или неуспех работы функции. Некоторые функции могут возвращать **-1** - обозначение пользовательского исключения, которое не является ошибкой работы программы. Например, функция `create_from_form` класса `Users` может вернуть **-1** - функция успешно завершила свою работу, программных ошибок не возникло, но пользователь не был создан: в форме ввода пароля и повторного ввода данные не совпали. Такие исключения обрабатываются при генерации страницы.
* В случае возникновения программной ошибки, запись об ошибке логгируется прямо в функции.
