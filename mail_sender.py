from mailer import Mailer, Message
from app import render_template, url_for, app
from uuid import uuid4

from app import Tokens, db, settings
import config


def generate_token():
    return str(uuid4())


def email_from_html(target_email, subject, html):
    msg = Message(From=config.MAILER_EMAIL, To=target_email, charset='utf-8')
    msg.Subject, msg.Html = subject, html
    try:
        sender.send(msg)
        return 1
    except Exception as e:
        app.logger.error(e)
        return 0


def send_verification_link(target_email):
    subject = "SharxDC: Завершение регистрации"
    token = generate_token()
    # check for another tokens
    obj = Tokens.query.filter_by(email=target_email).all()
    if obj:
        db.session.delete(*obj)
        db.session.commit()
    Tokens.add(target_email, token)
    return email_from_html(target_email, subject, render_template('mail/verify.html',
        link=url_for('verify', email=target_email, token=token, _external=True)))


def send_block_message(user_obj):
    subject = "SHARxDC: Вы заблокированы"
    target_email = user_obj.email
    email_from_html(target_email, subject, render_template('mail/text.html', text=settings.BLOCKED_USER_EMAIL_TEXT))


sender = Mailer(host=config.SMTP_SERVER, port=config.SMTP_PORT,
    use_ssl=config.SMTP_USE_SSL, use_tls=config.SMTP_USE_TLS,
    usr=config.MAILER_EMAIL, pwd=config.MAILER_PASSWORD)
