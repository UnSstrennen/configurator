from tempfile import NamedTemporaryFile
from xlsxwriter import Workbook


MONEY_FORMAT = '#,##0.00 [$$-409]'


def export(id, format):
    if format in ['.xls', '.xlsx']:
        return excel(id, format)


def write_headers_excel(wb, sheet, row, is_employee=True):
    headers = ['Part No.', 'Наименование', 'Кол-во',
    'Cost цена', 'Cost стоимость', 'Цена без НДС', 'НДС',
    'Стоимость', 'Стоимость с НДС']
    if not is_employee:
        headers.pop(3)
        headers.pop(3)
    for header in headers:
        style = {}
        if headers.index(header) == 0:
            style['left'] = 2
            style['right'] = 1
            style['bottom'] = 1
            style['top'] = 2
        elif headers.index(header) == 6 and not is_employee:
            style['left'] = 1
            style['right'] = 2
            style['bottom'] = 1
            style['top'] = 2
        elif headers.index(header) == 8 and is_employee:
            style['left'] = 1
            style['right'] = 2
            style['bottom'] = 1
            style['top'] = 2
        else:
            style['left'] = 1
            style['right'] = 1
            style['bottom'] = 1
            style['top'] = 2
        style['bold'] = True
        sheet.write(row, headers.index(header), header, wb.add_format(style))


def form_total_formula(col, rows):
    res = '=SUM('
    for row in rows:
        res += col + str(row) + ', '
    res = res[:-2] + ')'  # striping last ' ,'
    return res


def excel(id, format):
    from app import Orders, Equipment, Components, session, settings
    obj = Orders.query.filter_by(order_id=id).first()
    named_data = Orders.get_data(obj.data)
    if obj is None:
        return 0
    file = NamedTemporaryFile(suffix=format, delete=False)
    wb = Workbook(file.name)
    sheet = wb.add_worksheet()
    row = 0
    order_meta = {
        'Автор решения': 'author',
        'Заказчик': 'customer_name',
        'Контактное лицо': 'contact_name',
        'Контактный телефон': 'contact_phone',
        'Контактный email': 'contact_email',
        'Описание': 'description'
    }
    equipment_excel_rows = list()  # warning: formula rows, like row index + 1
    if session['role'] == 'employee':
        sheet.set_column('A:B', 20)
        sheet.set_column('D:F', 15)
        sheet.set_column('H:I', 15)
        sheet.merge_range('A1:I3', 'Sharx решение #' + Orders.__str__(id), wb.add_format({'border': 2, 'align': 'vcenter', 'font_size': 25}))
        row += 3
        for top_header in order_meta:
            if order_meta[top_header] == 'description':
                if not obj.description:
                    continue
                sheet.merge_range(row, 0, row+2, 2, top_header, wb.add_format({'bottom': 1, 'right': 1, 'left': 2, 'bold': True, 'valign': 'vcenter'}))
                sheet.merge_range(row, 3, row+2, 8, getattr(obj, order_meta[top_header]), wb.add_format({'bottom': 1, 'right': 2, 'valign': 'top'}))
                row += 3
            else:
                sheet.merge_range(row, 0, row, 2, top_header, wb.add_format({'bottom': 1, 'right': 1, 'left': 2, 'bold': True}))
                sheet.merge_range(row, 3, row, 8, getattr(obj, order_meta[top_header]), wb.add_format({'bottom': 1, 'right': 2}))
                row += 1
        write_headers_excel(wb, sheet, row, is_employee=True)
        row += 2
        cost_all_total = vat_all_total = price_all_total = price_all_vat_total = 0.0
        for category in obj.data:
            equipment_list = Equipment.query.filter(Equipment.equipment_id.in_(obj.data[category].keys())).all()
            row -= 1
            for equipment in equipment_list:
                eq_position = obj.data[category][str(equipment.equipment_id)]
                components = Components.query.filter(Components.component_id.in_(eq_position['components'].keys())).all()
                components_count = len(components)
                equipment_row = row
                sheet.set_row(equipment_row, None, None, {'collapsed': True})
                equipment_excel_rows.append(equipment_row+1)
                row += 1
                cost_total = cost_prices = price_prices = 0.0
                vat_total = price_total = price_vat_total = 0.0
                for component in components:
                    sheet.set_row(row, None, None, {'level': 1, 'hidden': False})
                    count = eq_position['components'][str(component.component_id)]
                    cost_prices += float(component.cost)
                    cost_total += float(component.cost) * count
                    price_prices += float(component.price)
                    vat_total += float(component.price) * (settings.VAT / 100) if component.vat else 0
                    price_total += float(component.price) * count
                    if component.vat:
                        price_vat_total += float(component.price * count) * float((100 + settings.VAT) / 100)
                    else:
                        price_vat_total += float(component.price)
                    data = [
                        component.part_number,
                        component.name,
                        count,
                        component.cost,
                        '=C%s*D%s' % (row+1, row+1),
                        component.price,
                        '=C{}*F{}*{}%'.format(row+1, row+1, settings.VAT) if component.vat else '0',
                        '=C%s*F%s' % (row+1, row+1),
                        '=(F%s+G%s)*C%s' % (row+1, row+1, row+1),
                    ]
                    for i in range(9):
                        style = {}
                        if i != 2:
                            style['num_format'] = MONEY_FORMAT
                        if components.index(component) == 0:
                            style['top'] = 1
                        if i == 0:
                            style['left'] = 2
                        if i == 8:
                            style['right'] = 2
                        if i in [0, 1, 2, 3, 5]:
                            sheet.write(row, i, data[i], wb.add_format(style))
                        else:
                            sheet.write_formula(row, i, data[i], wb.add_format(style))
                    row += 1
                cost_all_total += cost_total
                vat_all_total += vat_total
                price_all_total += price_total
                price_all_vat_total += price_vat_total
                data = [
                    equipment.part_number,
                    equipment.name,
                    int(eq_position['count']),
                    '=SUM(E%s:E%s)' % (equipment_row+2, equipment_row+1+components_count),
                    '=C%s*D%s' % (equipment_row+1, equipment_row+1),
                    '=SUM(H%s:H%s)' % (equipment_row+2, equipment_row+1+components_count),
                    '=SUM(G%s:G%s)' % (equipment_row+2, equipment_row+1+components_count),
                    '=C%s*F%s' % (equipment_row+1, equipment_row+1),
                    '=C%s*(F%s+G%s)' % (equipment_row+1, equipment_row+1, equipment_row+1)
                    ]
                for col in range(9):
                    style = {}
                    style['bottom'] = 1
                    style['top'] = 2
                    if col == 0:
                        style['left'] = 2
                    elif col == 8:
                        style['right'] = 2
                    if col > 2:
                        style['num_format'] = MONEY_FORMAT
                        sheet.write_formula(equipment_row, col, data[col], wb.add_format(style))
                    else:
                        sheet.write(equipment_row, col, data[col], wb.add_format(style))
                row += 1
    elif session['role'] == 'partner':
        sheet.set_column('A:B', 20)
        sheet.set_column('D:G', 15)
        sheet.merge_range('A1:G3', 'Sharx решение #' + Orders.__str__(id), wb.add_format({'border': 2, 'align': 'vcenter', 'font_size': 25}))
        row += 3
        for top_header in order_meta:
            if order_meta[top_header] == 'description':
                if not obj.description:
                    continue
                sheet.merge_range(row, 0, row+2, 2, top_header, wb.add_format({'bottom': 1, 'right': 1, 'left': 2, 'bold': True, 'valign': 'vcenter'}))
                sheet.merge_range(row, 3, row+2, 6, getattr(obj, order_meta[top_header]), wb.add_format({'bottom': 1, 'right': 2, 'valign': 'top'}))
                row += 3
            else:
                sheet.merge_range(row, 0, row, 2, top_header, wb.add_format({'bottom': 1, 'right': 1, 'left': 2, 'bold': True}))
                sheet.merge_range(row, 3, row, 6, getattr(obj, order_meta[top_header]), wb.add_format({'bottom': 1, 'right': 2}))
                row += 1
        write_headers_excel(wb, sheet, row, is_employee=False)
        row += 2
        cost_all_total = vat_all_total = price_all_total = price_all_vat_total = 0.0
        for category in obj.data:
            equipment_list = Equipment.query.filter(Equipment.equipment_id.in_(obj.data[category].keys())).all()
            row -= 1
            for equipment in equipment_list:
                eq_position = obj.data[category][str(equipment.equipment_id)]
                components = Components.query.filter(Components.component_id.in_(eq_position['components'].keys())).all()
                components_count = len(components)
                equipment_row = row
                sheet.set_row(equipment_row, None, None, {'collapsed': True})
                equipment_excel_rows.append(equipment_row+1)
                row += 1
                cost_total = cost_prices = price_prices = 0.0
                vat_total = price_total = price_vat_total = 0.0
                for component in components:
                    sheet.set_row(row, None, None, {'level': 1, 'hidden': False})
                    count = eq_position['components'][str(component.component_id)]
                    cost_prices += float(component.cost)
                    cost_total += float(component.cost) * count
                    price_prices += float(component.price)
                    vat_total += float(component.price) * (settings.VAT / 100) if component.vat else 0
                    price_total += float(component.price) * count
                    if component.vat:
                        price_vat_total += float(component.price * count) * float((100 + settings.VAT) / 100)
                    else:
                        price_vat_total += float(component.price)
                    data = [
                        component.part_number,
                        component.name,
                        count,
                        component.price,
                        '=C{}*D{}*{}%'.format(row+1, row+1, settings.VAT) if component.vat else '0',
                        '=C%s*D%s' % (row+1, row+1),
                        '=(D%s+E%s)*C%s' % (row+1, row+1, row+1),
                    ]
                    for i in range(7):
                        style = {}
                        if i != 2:
                            style['num_format'] = MONEY_FORMAT
                        if components.index(component) == 0:
                            style['top'] = 1
                        if i == 0:
                            style['left'] = 2
                        if i == 6:
                            style['right'] = 2
                        if i in [0, 1, 2, 3, 5]:
                            sheet.write(row, i, data[i], wb.add_format(style))
                        else:
                            sheet.write_formula(row, i, data[i], wb.add_format(style))
                    row += 1
                cost_all_total += cost_total
                vat_all_total += vat_total
                price_all_total += price_total
                price_all_vat_total += price_vat_total
                data = [
                    equipment.part_number,
                    equipment.name,
                    int(eq_position['count']),
                    '=SUM(F%s:F%s)' % (equipment_row+2, equipment_row+1+components_count),
                    '=SUM(E%s:E%s)' % (equipment_row+2, equipment_row+1+components_count),
                    '=C%s*D%s' % (equipment_row+1, equipment_row+1),
                    '=C%s*(D%s+E%s)' % (equipment_row+1, equipment_row+1, equipment_row+1)
                    ]
                for col in range(7):
                    style = {}
                    style['bottom'] = 1
                    style['top'] = 2
                    if col == 0:
                        style['left'] = 2
                    elif col == 6:
                        style['right'] = 2
                    if col > 2:
                        style['num_format'] = MONEY_FORMAT
                        sheet.write_formula(equipment_row, col, data[col], wb.add_format(style))
                    else:
                        sheet.write(equipment_row, col, data[col], wb.add_format(style))
                row += 1
    if session['role'] == 'employee':
        # make bottom border
        row -= 1
        [sheet.write(row, i, None, wb.add_format({'top': 2})) for i in range(9)]
        row += 2
        total_headers = ['Cost итог:', 'Итого без НДС:', 'Сумма НДС:', 'Итого с НДС:']
        [sheet.write(r, 7, total_headers[r - row], wb.add_format({'bold': True})) for r in range(row, row + len(total_headers))]
        totals = [
            form_total_formula('E', equipment_excel_rows),
            form_total_formula('H', equipment_excel_rows),
            form_total_formula('G', equipment_excel_rows),
            form_total_formula('I', equipment_excel_rows)
        ]
        [sheet.write_formula(r, 8, totals[r - row], wb.add_format({'num_format': MONEY_FORMAT})) for r in range(row, row + len(totals))]
    elif session['role'] == 'partner':
        # make bottom border
        row -= 1
        [sheet.write(row, i, None, wb.add_format({'top': 2})) for i in range(7)]
        row += 2
        total_headers = ['Итого без НДС:', 'Сумма НДС:', 'Итого с НДС:']
        [sheet.write(r, 5, total_headers[r - row], wb.add_format({'bold': True})) for r in range(row, row + len(total_headers))]
        totals = [form_total_formula('F', equipment_excel_rows), form_total_formula('E', equipment_excel_rows), form_total_formula('G', equipment_excel_rows)]
        [sheet.write(r, 6, totals[r - row], wb.add_format({'num_format': MONEY_FORMAT})) for r in range(row, row + len(totals))]
    wb.close()
    return file
