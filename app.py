from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm.attributes import flag_modified
from flask import Flask, session, render_template, redirect, request, url_for, flash, abort, Response, jsonify, send_file, send_from_directory
from flask_fontawesome import FontAwesome
from werkzeug.security import check_password_hash, generate_password_hash
from jsonpickle import encode as jsonpickle
import logging
from datetime import datetime, timedelta
from os import makedirs
from os.path import exists
from copy import deepcopy
import json
import urllib.parse

import config
from setter import settings
from exporter import export
from logger import schedule_log_archiving


def create_engine():
    """ engine string generator """
    engine_str = '{}+{}://{}:{}@{}:{}/{}'.format(config.DATABASE_TYPE,
                                                 config.CONNECTOR, config.USERNAME, config.PASSWORD,
                                                 config.DATABASE_HOST, config.DATABASE_PORT, config.DATABASE)
    return engine_str


# app & modules initialization
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = create_engine()
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = config.SECRET_KEY
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(minutes=config.SESSION_LIFETIME)
app.config['DEBUG'] = config.DEBUG
if not exists('logs'):
    makedirs('logs')
logging.basicConfig(level=logging.INFO, filename='logs/log.log', format='%(levelname)s:(%(name)s) %(message)s')
db = SQLAlchemy(app)
db.init_app(app)
fa = FontAwesome(app)


def create_json():
    """ creates category.json if not exists """
    if not exists('json/categories.json'):
        if not exists('json'):
            makedirs('json')
        with open('json/categories.json', mode='w') as f:
            json.dump({"equipment": [], "components": {}}, f)
    else:
        with open('json/categories.json') as f:
            if len(f.read()) == 0:
                json.dump({"equipment": [], "components": {}}, f)


# Database tables description & methods for query objs

class Users(db.Model):
    """ DB table Users model """
    user_id = db.Column(db.Integer, primary_key=True)
    password_hash = db.Column(db.String(100))
    username = db.Column(db.String(100), unique=True)
    surname = db.Column(db.String(100))
    name = db.Column(db.String(100))
    email = db.Column(db.String(100))
    company = db.Column(db.String(100))
    phone = db.Column(db.String(30))
    verified = db.Column(db.Boolean, default=False)
    active = db.Column(db.Boolean, default=False)
    role = db.Column(db.String(8))
    last_entry = db.Column(db.DateTime(timezone=True))
    level = db.Column(db.Integer)
    orders = db.relationship('Orders', backref='users',
                             cascade="all, delete-orphan")

    @classmethod
    def check_user(self, username, password):
        """
        Checks & log in user, if data is correct
        args:
            - username (str)
            - password (str) - compares with hash of user, if <username> exists
        returns user database ID if data is correct and user successfully logged in,
        returns 0 if data is correct, but user is inactive
        returns -1 if data is incorrect
        """
        user_obj = self.query.filter_by(username=username).first()
        if user_obj is None:
            return -1
        if check_password_hash(user_obj.password_hash, password):
            if not user_obj.active:
                return 0
            user_obj.last_entry = datetime.now()
            db.session.add(user_obj)
            db.session.commit()
            return user_obj.user_id
        else:
            return -1


    @classmethod
    def delete_user(self, id):
        """
        Delete user if exists & delegate all orders of deleted user to current user
        args:
            - id (int) - user id in database
        returns None
        """
        obj = Users.query.filter_by(user_id=id).first()
        if obj is not None:
            orders = Orders.query.filter_by(user_id=id).all()
            if orders is not None:
                for order in orders:
                    order.user_id = session['user_id']
                    db.session.add(order)
            db.session.delete(obj)
        db.session.commit()

    @classmethod
    def block_user(self, id):
        """
        Block user if exists
        args:
            - id (int) - user id in database
        returns None
        """
        obj = self.query.filter_by(user_id=id).first()
        if obj is not None:
            obj.active = not obj.active
            if not obj.active:
                # send email about blocking
                from mail_sender import send_block_message
                send_block_message(obj)
            db.session.add(obj)
        db.session.commit()

    @classmethod
    def get_role(self, id):
        """
        Returns user role
        args:
            - id (int) 0 user id in database
        """
        obj = self.query.filter_by(user_id=id).first()
        return obj.role

    @classmethod
    def get_username(self, id):
        """
        Returns username
        args:
            - id (int) 0 user id in database
        """
        obj = self.query.filter_by(user_id=id).first()
        return obj.username

    @classmethod
    def edit_from_form(self, form):
        """
        Returns user role
        args:
            - id (int) 0 user id in database
        """
        user_id = int(form['user_id'])
        user = self.query.filter_by(user_id=user_id).first()
        if user is None:
            return 0
        user.name = form['name']
        user.surname = form['surname']
        user.company = form['company']
        if user.email != form['email']:
            user.verified = False
            from mail_sender import send_verification_link
            send_verification_link(form['email'])
        user.email = form['email']
        user.phone = form['phone']
        old_password = form['old_password']
        new_password = form['new_password']
        repeat_new_password = form['repeat_new_password']
        if old_password or new_password or repeat_new_password:
            if not check_password_hash(user.password_hash, old_password):
                return -1
            if new_password != repeat_new_password:
                return -1
            user.password_hash = generate_password_hash(new_password)
        try:
            db.session.add(user)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def create_from_form(self, form):
        """
        Creates user using data from request.form
        args:
            - form (ImmutableMultiDict) - request.form object
                - username (str) - uses for logging in
                - surname (str)
                - name (str)
                - company (str)
                - email (str)
                - phone (str)
                - password (str)
                - repeat_password (str) - must be same as password
        returns -1 if password doesn't match with repeat_password
        return 0 if error occures
        returns 1 if user edited successfully
        """
        password = form['password']
        repeat_password = form['repeat_password']
        if password != repeat_password:
            return -1
        password_hash = generate_password_hash(password)
        username = form['username']
        name = form['name']
        surname = form['surname']
        company = form['company']
        email = form['email']
        phone = form['phone']
        if email.rstrip().endswith(config.EMPLOYEE_MAIL_DOMAIN):
            level = 100
            role = 'employee'
        else:
            level = 1
            role = 'partner'
        try:
            user_obj = Users(password_hash=password_hash,
                             username=username,
                             name=name,
                             surname=surname,
                             company=company,
                             email=email,
                             phone=phone,
                             role=role,
                             level=level,
                             last_entry=None)
            db.session.add(user_obj)
            db.session.commit()
            return user_obj.user_id
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def get_users_for_select(self):
        res = self.query.filter_by(role='partner', active=True).order_by(self.company).all()
        if res is None:
            return []
        return res


class Tokens(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100))
    token = db.Column(db.String(36), unique=True)

    @classmethod
    def add(self, email, token):
        obj = Tokens(email=email, token=token)
        db.session.add(obj)
        db.session.commit()

    @classmethod
    def check(self, email, token):
        obj = self.query.filter_by(email=email, token=token).first()
        if obj is not None:
            # mark verification in user row in Users model
            user_obj = Users.query.filter_by(email=email).first()
            user_obj.verified = True
            if email.endswith(config.EMPLOYEE_MAIL_DOMAIN):
                user_obj.active = True
            db.session.add(user_obj)
            db.session.delete(obj)
            db.session.commit()
            return 1
        else:
            return 0


class Orders(db.Model):
    order_id = db.Column(db.Integer, primary_key=True)
    base_order_id = db.Column(db.Integer, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.user_id'))
    author = db.Column(db.String(255))
    order_name = db.Column(db.String(255))
    customer_name = db.Column(db.String(255))
    contact_name = db.Column(db.String(255))
    contact_phone = db.Column(db.String(30))
    contact_email = db.Column(db.String(255))
    description = db.Column(db.Text)
    data = db.Column(db.JSON, default=None)
    created_on = db.Column(db.Date)
    last_modified = db.Column(db.Date)
    previous_orders = db.Column(db.String(3000))

    def __str__(self):
        if type(self) in [str, int]:
            self = Orders.query.filter_by(order_id=self).first()
        if self.base_order_id is None:
            return str(self.order_id) + '.0'
        else:
            return str(self.base_order_id) + '.' + str(len(self.previous_orders.split()))

    @classmethod
    def get_new_str_order(self, order):
        """
        Returns stringified order of the new version of order (like it's new
        order saved as LAST version of order)
        args:
            - order (SQLAlchemy object) - order for which operation is executed
        """
        id = order.base_order_id if order.base_order_id is not None else order.order_id
        return str(id) + '.' + str(len(order.previous_orders) + 1)

    @classmethod
    def get_orders(self, user_id=None):
        objs = self.query.filter_by(user_id=user_id).order_by(self.order_id).all()
        if objs is None or user_id is None:
            return 0
        res = dict()
        for obj in objs:
            if obj.base_order_id is not None:
                if obj.base_order_id not in res:
                    res[obj.base_order_id] = list()
                res[obj.base_order_id].append(obj)
            else:
                if obj.order_id not in res:
                    res[obj.order_id] = list()
                res[obj.order_id].append(obj)
        for lis in res:
            res[lis].sort(key=lambda x:len(x.previous_orders), reverse=True)
        return res

    @classmethod
    def create_from_form(self, form):
        order_name = form['order_name']
        customer_name = form['customer_name']
        contact_name = form['contact_name']
        contact_phone = form['contact_phone']
        contact_email = form['contact_email']
        description = form['description']
        try:
            obj = Orders(order_name=order_name,
                         customer_name=customer_name,
                         contact_name=contact_name,
                         contact_phone=contact_phone,
                         contact_email=contact_email,
                         description=description,
                         user_id=session['user_id'],
                         author=Users.get_username(session['user_id']),
                         base_order_id = None,
                         data={},
                         previous_orders='',
                         created_on=datetime.date(datetime.now()),
                         last_modified=datetime.date(datetime.now()))
            db.session.add(obj)
            db.session.commit()
            return obj.order_id
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def save(self, form, cookies):
        try:
            base_order = self.query.filter_by(order_id=int(form['order_id'])).first()
            if base_order is None:
                return 0
            if form['save_as'] == 'as_current':
                # save like this version
                base_order.data = eval(urllib.parse.unquote(cookies['order']))
                flag_modified(base_order, 'data')  # because of the JSON
            elif form['save_as'] == 'as_new':
                # save like new version of the same order
                new_base_order_id = base_order.order_id if base_order.base_order_id is None else base_order.base_order_id
                new_previous_orders = ' '.join(base_order.previous_orders.split() + [str(base_order.order_id)])
                obj = Orders(order_name=base_order.order_name,
                             customer_name=base_order.customer_name,
                             contact_name=base_order.contact_name,
                             contact_phone=base_order.contact_phone,
                             contact_email=base_order.contact_email,
                             description=base_order.description,
                             user_id=session['user_id'],
                             author=base_order.author,
                             base_order_id = new_base_order_id,
                             data=eval(urllib.parse.unquote(cookies['order'])),
                             previous_orders=new_previous_orders,
                             created_on=datetime.date(datetime.now()),
                             last_modified=datetime.date(datetime.now()))
                db.session.add(obj)
            elif form['save_as'] == 'as_another_user':
                obj = Orders(order_name=base_order.order_name,
                             customer_name=base_order.customer_name,
                             contact_name=base_order.contact_name,
                             contact_phone=base_order.contact_phone,
                             contact_email=base_order.contact_email,
                             description=base_order.description,
                             user_id=form['user_id'],
                             author=base_order.author,
                             base_order_id = None,
                             data=eval(urllib.parse.unquote(cookies['order'])),
                             previous_orders='',
                             created_on=datetime.date(datetime.now()),
                             last_modified=datetime.date(datetime.now()))
                db.session.add(obj)
            else:
                return 0
            base_order.last_modified = datetime.date(datetime.now())
            db.session.add(base_order)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def delete(self, id):
        try:
            obj = self.query.filter_by(order_id=id).first()
            if obj is None:
                return 0
            previous_orders = self.query.filter(Orders.order_id.in_(obj.previous_orders.split())).all()
            if previous_orders is not None:
                [db.session.delete(order) for order in previous_orders]
            db.session.delete(obj)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def delete_equipment(self, order_id, equipment_id):
        """
        Deletes equipment from order.
        args:
            - order_id (int) - order containing deleting equipment
            - equipment_id (int) - id of equipment which should be deleted
        returns 1 if ok, 0 if error
        """
        try:
            order = self.query.filter_by(order_id=order_id).first()
            if order is None:
                return 0
            deleted = False
            for category in order.data:
                if str(equipment_id) in order.data[category]:
                    del order.data[category][str(equipment_id)]
                    if not order.data[category]:
                        del order.data[category]
                    deleted = True
                    break
            if not deleted:
                raise KeyError("order doesn't contain " + str(equipment_id))
            flag_modified(order, 'data')
            db.session.add(order)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def configure_equipment(self, order_id, equipment_id, form):
        try:
            order = self.query.filter_by(order_id=order_id).first()
            if order is None:
                return 0
            component_ids = form.getlist('component_ids')
            equipment_data = order.data[Equipment.get_category(equipment_id)][str(equipment_id)]
            equipment_data['configuration'] = component_ids
            flag_modified(order, 'data')
            db.session.add(order)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def equipment_in_order(self, order_id, equipment_id):
        """
        Returns True if equipment in order, else - False
        args:
            - order_id (int) - order to check for equipment existance
            - equipment_id (int) - id of equipment to check
        """
        order = Orders.query.filter_by(order_id=order_id).first()
        for category in order.data:
            if str(equipment_id) in order.data[category]:
                return True
        return False

    @classmethod
    def get_data(self, data_got):
        # TODO: fix docs
        data = deepcopy(data_got)
        """
        Returns a "named" obj order (replaces ids, adds component data and
            equipment names)
        args:
            - order (SQLAlchemy object) - order object
        returns dict in format like:
        {
            'category_name': {
                'equipment_1_name': {
                    'count': 1,
                    'id': 3,
                    'total': 100,
                    'vat_total': 120,
                    'end_of_sale': False
                }
            }
        }
        """
        for category in data:
            equipment = Equipment.query.filter(Equipment.equipment_id.in_(data[category].keys())).all()
            for equip in equipment:
                data[category][equip.name] = data[category][str(equip.equipment_id)]
                component_ids = dict(data[category][str(equip.equipment_id)]['components'])
                total, vat_total = self.count_totals(component_ids)
                end_of_sale = Components.end_of_sale_in_list(component_ids)
                data[category][equip.name]['id'] = equip.equipment_id
                data[category][equip.name]['total'] = total
                data[category][equip.name]['vat_total'] = vat_total
                data[category][equip.name]['end_of_sale'] = end_of_sale
                del data[category][str(equip.equipment_id)]
        return data

    @classmethod
    def count_totals(self, id_list):
        """
        id_list format:
            {
                '<component_id>': <count>
            }
        """
        obj_list = Components.query.filter(Components.component_id.in_(id_list.keys())).all()
        total, vat_total = 0, 0
        for obj in obj_list:
            price = float(obj.price)
            count = int(id_list[str(obj.component_id)])
            total += price * count
            if obj.vat:
                vat_total += price * (1 + settings.VAT / 100) * count
            else:
                vat_total += price * count
        return total, vat_total

    @classmethod
    def edit_from_form(self, form):
        obj = self.query.filter_by(order_id = form['order_id']).first()
        if obj is None:
            return 0
        try:
            obj.order_name = form['order_name']
            obj.customer_name = form['customer_name']
            obj.contact_name = form['contact_name']
            obj.contact_phone = form['contact_phone']
            obj.contact_email = form['contact_email']
            obj.description = form['description']
            db.session.add(obj)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0


class Equipment(db.Model):
    equipment_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    part_number = db.Column(db.String(255))
    category = db.Column(db.String(255))
    end_of_sale = db.Column(db.Boolean, default=False)
    requirements = db.Column(db.JSON, default={})

    @classmethod
    def create_from_form(self, form):
        try:
            obj = Equipment(name=form['name'],
                part_number=form['part_number'],
                category=form['category']
                )
            db.session.add(obj)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def get_name(self, equipment_id):
        return self.query.filter_by(equipment_id=equipment_id).first().name

    @classmethod
    def get_category(self, equipment_id):
        obj = self.query.filter_by(equipment_id=equipment_id).first()
        if obj is None:
            return 0
        else:
            return obj.category

    @classmethod
    def edit_category_from_form(self, form):
        try:
            # update json
            create_json()
            with open('json/categories.json') as f:
                categories = json.load(f)
            categories['equipment'].remove(form['category'])
            categories['equipment'].append(form['new_category_name'])
            with open('json/categories.json', mode='w') as f:
                json.dump(categories, f)
            # update categories in DB
            objs = self.query.filter_by(category=form['category']).all()
            for obj in objs:
                obj.category = form['new_category_name']
            db.session.add_all(objs)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def get_first_category(self):
        create_json()
        with open('json/categories.json', mode='r') as f:
            categories = json.load(f)
            categories = categories['equipment']
            if not categories:
                return str(None)
            else:
                return categories[0]

    @classmethod
    def get_categories(self, exist=False):
        # TODO: actions if not exist as in components
        create_json()
        with open('json/categories.json') as f:
            res = json.load(f)['equipment']
            if exist:
                for cat in res:
                    if self.query.filter_by(category=cat).first() is None:
                        res.remove(cat)
            return res

    @classmethod
    def add_category(self, category):
        try:
            create_json()
            with open('json/categories.json') as f:
                data = json.load(f)
                data['equipment'].append(category)
            with open('json/categories.json', mode='w') as f:
                json.dump(data, f)
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def delete_category(self, category):
        try:
            create_json()
            with open('json/categories.json') as f:
                data = json.load(f)
                data['equipment'].remove(category)
            with open('json/categories.json', mode='w') as f:
                json.dump(data, f)
            # remove from database
            containing_rows = self.query.filter_by(category=category).all()
            [db.session.delete(row) for row in containing_rows]
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def serialize(self):
        create_json()
        categories = self.get_categories(exist=True)
        res = dict()
        for category in categories:
            res[category] = dict()
        for equip in self.query.all():
            if equip.end_of_sale:
                continue
            res[equip.category][equip.name] = {'id': equip.equipment_id, 'requirements': equip.requirements}
        return res

    @classmethod
    def edit_requirement_from_form(self, form):
        try:
            equipment_id = form['equipment_id']
            component = form['component']
            component_option = form['component_option']
            operator = form['operator']
            operand = form['operand']
            obj = self.query.filter_by(equipment_id=equipment_id).first()
            if obj is None:
                return 0
            filters = obj.requirements[component]['filters']
            for filter in filters:
                if filter['component_option'] == component_option:
                    filter['operator'] = operator
                    filter['value'] = operand
                    break
            obj.requirements[component]['filters'] = filters
            flag_modified(obj, 'requirements')
            db.session.add(obj)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def update_from_form(self, form):
        try:
            obj = self.query.filter_by(equipment_id=form['equipment_id']).first()
            if obj is None:
                return 0
            obj.name = form['name']
            obj.part_number = form['part_number']
            obj.end_of_sale = 'end_of_sale' in form
            db.session.add(obj)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def delete(self, id):
        try:
            obj = self.query.filter_by(equipment_id = id).first()
            if obj is None:
                return 0
            db.session.delete(obj)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def add_requirement_from_form(self, form):
        id = form['equipment_id']
        component = form['component']
        obj = self.query.filter_by(equipment_id=id).first()
        if obj is None:
            return 0
        try:
            meta = obj.requirements
            meta[component] = {"max": 1, "filters": []}
            obj.requirements = meta
            flag_modified(obj, 'requirements')  # because of the JSON
            db.session.add(obj)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def update_max_from_form(self, form):
        try:
            id = form['equipment_id']
            component = form['component']
            max = form['max']
            obj = self.query.filter_by(equipment_id=id).first()
            if obj is None:
                return 0
            meta = obj.requirements
            meta[component]['max'] = int(max)
            obj.requirements = meta
            flag_modified(obj, 'requirements')  # because of the JSON
            db.session.add(obj)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def add_filter(self, form):
        try:
            id = form['equipment_id']
            component = form['component']
            obj = self.query.filter_by(equipment_id=id).first()
            if obj is None:
                return 0
            meta = obj.requirements
            requirement = meta[component]
            if 'filters' not in requirement.keys():
                requirement['filters'] = list()
            requirement['filters'].append({
                "component_option": form['component_option'],
                "dimension": Components.get_dimension(component, form['component_option']),
                "operator": form['operator'],
                "value": form['value']
            })
            requirement = meta[component]
            obj.requirements = meta
            flag_modified(obj, 'requirements')  # because of the JSON
            db.session.add(obj)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def delete_requirement(self, form):
        try:
            id = form['equipment_id']
            requirement = form['requirement']
            obj = self.query.filter_by(equipment_id=id).first()
            if obj is None:
                return 0
            del obj.requirements[requirement]
            flag_modified(obj, 'requirements')
            db.session.add(obj)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def delete_filter(self, form):
        try:
            id = form['equipment_id']
            component = form['component']
            filter_to_delete = form['component_option']
            obj = self.query.filter_by(equipment_id=id).first()
            if obj is None:
                return 0
            meta = obj.requirements
            filters = meta[component]['filters']
            for filter in filters:
                if filter['component_option'] == filter_to_delete:
                    filters.remove(filter)
                    break
            meta[component]['filters'] = filters
            obj.requirements = meta
            flag_modified(obj, 'requirements')  # because of the JSON
            db.session.add(obj)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0


class Components(db.Model):
    component_id = db.Column(db.Integer, primary_key=True)
    part_number = db.Column(db.String(255))
    name = db.Column(db.String(255))
    description = db.Column(db.Text)
    category = db.Column(db.String(255))
    cost = db.Column(db.Numeric(12, 2))
    price = db.Column(db.Numeric(12, 2))
    price_last_modified = db.Column(db.Date)
    meta = db.Column(db.JSON, default={})
    end_of_sale = db.Column(db.Boolean, default=False)
    vat = db.Column(db.Boolean)

    @classmethod
    def add_category(self, form):
        try:
            options_dict = dict()
            category = form['category']
            option_ind = 0
            create_json()
            with open('json/categories.json') as f:
                data = json.load(f)
            if category in data['components'].keys():
                return None  # TODO: error if category already exists
            while 'option_' + str(option_ind) in form:
                options_dict[form['option_' + str(option_ind)]] = form['option_dimension_' + str(option_ind)]
                option_ind += 1
            data['components'][category] = options_dict
            with open('json/categories.json', mode='w') as f:
                json.dump(data, f)
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def get_categories(self, exist=False):
        create_json()
        with open('json/categories.json') as f:
            res = json.load(f)['components']
            if exist:
                for cat in res:
                    if self.query.filter_by(category=cat).first() is None:
                        del res[cat]
            return res

    @classmethod
    def get_options(self, category):
        """
        Returns options & dimensions as dict:
        {
            'option1': 'dimension1',
            'option2': 'dimension2'
        }
        args:
            - category (str) - category for choosing options&dimensions
        """
        create_json()
        with open('json/categories.json') as f:
            res = json.load(f)['components']
            if res:
                return res[category]
            else:
                return dict()

    @classmethod
    def edit_category_from_form(self, category, form):
        create_json()
        try:
            if 'oldvalue' in form:
                old, new = form['oldvalue'], form['newvalue']
                if old == new:
                    return 1
            to_change = form['changed']
            with open('json/categories.json') as f:
                categories = json.load(f)
            objs = self.query.filter_by(category=category).all()
            if to_change == 'name':
                # changed category name
                old = category
                new = form['category']
                for obj in objs:
                    obj.category = new
                    db.session.add(obj)
                categories['components'][new] = categories['components'][old]
                del categories['components'][old]
            elif to_change == 'option_name':
                # changed option name
                # make changes in json
                if old in categories['components'][category]:
                    # exists, change name of option
                    categories['components'][category][new] = categories['components'][category][old]
                    del categories['components'][category][old]
                for eq in Equipment.query.all():
                    if category in eq.requirements:
                        for filter in eq.requirements[category]['filters']:
                            if filter['component_option'] == str(old):
                                filter['component_option'] = str(new)
                                flag_modified(eq, 'requirements')
                                db.session.add(eq)
                else:
                    # doesn't exist, add new option
                    categories['components'][category][new] = ''
                # make changes in existing components
                for obj in objs:
                    if old in obj.meta:
                        # exists, change name of option
                        obj.meta[new] = obj.meta[old]
                        del obj.meta[old]
                    else:
                        obj.meta[new] = {'dimension': '', 'value': ''}
                    flag_modified(obj, 'meta')  # because of the JSON
            else:
                # changed dimension
                option = form['option']
                # make changes in json
                categories['components'][category][option] = new;
                # make changes in existing components
                for obj in objs:
                    obj.meta[option]['dimension'] = new
                    flag_modified(obj, 'meta')  # because of the JSON
            with open('json/categories.json', mode="w") as f:
                json.dump(categories, f)
            db.session.add_all(objs)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0


    @classmethod
    def delete_option(self, category, form):
        create_json()
        try:
            to_delete = form['option']
            with open('json/categories.json') as f:
                categories = json.load(f)
            objs = self.query.filter_by(category=category).all()
            option = form['option']
            # make changes in json
            del categories['components'][category][option]
            # make changes in existing components
            for obj in objs:
                del obj.meta[option]
                flag_modified(obj, 'meta')  # because of the JSON
            with open('json/categories.json', mode="w") as f:
                json.dump(categories, f)
            db.session.add_all(objs)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0


    @classmethod
    def get_first_category(self):
        create_json()
        with open('json/categories.json') as f:
            categories = json.load(f)
            categories = list(categories['components'].keys())
            if not categories:
                return str(None)
            else:
                return categories[0]

    @classmethod
    def fill_meta(self, category):
        create_json()
        res = dict()
        with open('json/categories.json') as f:
            options = json.load(f)['components'][category]
        for option in options:
            dimension = options[option]
            res[option] = {'dimension': dimension, "value": ""}
        return res

    @classmethod
    def create_from_form(self, form):
        try:
            obj = Components(
                part_number=form['part_number'],
                name=form['name'],
                description=form['description'],
                category=form['category'],
                price=form['price'],
                cost=form['cost'],
                vat='vat' in form,
                meta = self.fill_meta(form['category']),
                price_last_modified = datetime.date(datetime.now())
            )
            db.session.add(obj)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def update_from_form(self, form):
        try:
            obj = self.query.filter_by(component_id = form['component_id']).first()
            if obj is None:
                return 0
            obj.name = form['name']
            obj.part_number = form['part_number']
            obj.cost = form['cost']
            obj.price = form['price']
            obj.vat = 'vat' in form
            obj.end_of_sale = 'end_of_sale' in form
            obj.description = form['description']
            db.session.add(obj)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def update_option_from_form(self, form):
        # TODO: also update in equipment requirements
        try:
            obj = self.query.filter_by(component_id = form['component_id']).first()
            if obj is None:
                return 0
            meta = obj.meta
            meta[form['option']] = {'dimension': form['dimension'], 'value': form['value']}
            obj.meta = meta
            flag_modified(obj, 'meta')  # because of the JSON
            db.session.add(obj)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def delete(self, id):
        try:
            obj = self.query.filter_by(component_id = id).first()
            if obj is None:
                return 0
            # check if component is used in any order
            orders = Orders.query.all()
            if orders is not None:
                for order in orders:
                    for category in order.data:
                        for equipment in order.data[category]:
                            if id in order.data[category][equipment]['components'].values():
                                # used, can't delete
                                return -1
            db.session.delete(obj)
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def delete_category(self, category):
        try:
            # check if this category is used in any equipment
            equipment = Equipment.query.all()
            for equip in equipment:
                if category in equip.requirements and self.get_acceptable(category, equip.requirements[category]):
                    return -1
            create_json()
            with open('json/categories.json') as f:
                data = json.load(f)
                del data['components'][category]
            with open('json/categories.json', mode='w') as f:
                json.dump(data, f)
            # remove from database
            containing_rows = self.query.filter_by(category=category).all()
            [db.session.delete(row) for row in containing_rows]
            db.session.commit()
            return 1
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return 0

    @classmethod
    def get_acceptable(self, category, requirement):
        """
        Returns a list of components' SQLAlchemy objects,
        which approach to requirement
        args:
            - category (str) - category of component, among which the choice is made
            - requirement (dict) - requirement data formatted like:
                {
                "max": 1,
                "filters": [
                  {
                    "component_option": "Объём памяти",
                    "dimension": "ТБ",
                    "opreator": "more_eq",
                    "value": 40
                  }
                ]
              }
        * returns None if error occures
        """
        try:
            components = self.query.filter_by(category=category).order_by(self.name).all()
            res = list()
            for component in components:
                if component.end_of_sale:
                    continue
                acceptable = True
                for filter in requirement['filters']:
                    component_value = str(component.meta[filter['component_option']]['value'])
                    threshold_value = str(filter['value'])
                    if component_value.isdigit():
                        component_value = int(component_value)
                    if threshold_value.isdigit():
                        threshold_value = int(threshold_value)
                    if not component_value or not threshold_value:
                        acceptable = False
                        break
                    operator = filter['operator']
                    if operator == 'more_eq':
                        if not component_value >= threshold_value:
                            acceptable = False
                            break
                    elif operator == 'less_eq':
                        if not component_value <= threshold_value:
                            acceptable = False
                            break
                    elif operator == 'eq':
                        if not component_value == threshold_value:
                            acceptable = False
                            break
                    else:
                        raise ValueError('invalid operator: ' + operator)
                if acceptable:
                    res.append(component)
            return res
        except Exception as e:
            app.logger.error(e, exc_info=True)
            return None

    @classmethod
    def get_dimension(self, component, option):
        create_json()
        with open('json/categories.json') as f:
            return json.load(f)['components'][component][option]

    @classmethod
    def end_of_sale_in_list(self, id_list):
        obj_list = self.query.filter(Components.component_id.in_(id_list)).all()
        for obj in obj_list:
            if obj.end_of_sale:
                return True
        return False

    @classmethod
    def warning(self, obj):
        """ Returns True if one of the required fields is not filled """
        return not all([obj.name, obj.part_number, obj.cost, obj.price])


db.create_all()  # creates tables if not exist
