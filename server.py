from app import *
from mail_sender import send_verification_link


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    if 'user_id' not in session:
        return redirect('/login')
    else:
        return redirect('/order_manager')


@app.route('/equipment_manager', methods=['GET'])
def equipment_manager_redirect():
    if 'role' not in session or session['role'] != 'employee':
        return redirect('/')
    return redirect(url_for('equipment_manager', category=Equipment.get_first_category()))


@app.route('/equipment_manager/<string:category>', methods=['GET'])
def equipment_manager(category):
    if 'role' not in session or session['role'] != 'employee':
        return redirect('/')
    equipment_categories = Equipment.get_categories()
    return render_template('equipment_manager.html',
        equipment_categories = equipment_categories,
        component_categories = Components.get_categories(),
        equipment = Equipment.query.filter_by(category=category).all(),
        selected_category = category)


@app.route('/equipment', methods=['POST'])
def equipment():
    if 'role' not in session or session['role'] != 'employee':
        return redirect('/')
    res = Equipment.create_from_form(request.form)
    if res == 0:
        flash('Произошла ошибка. Попробуйте перезагрузить страницу', 'danger')
    else:
        return redirect(url_for('equipment_manager', category=request.form['category']))
    return redirect('/equipment_manager')


@app.route('/equipment/<int:id>', methods=['PUT', 'DELETE'])
def equipment_arg(id):
    if 'role' not in session or session['role'] != 'employee':
        return redirect('/')
    if request.method == 'PUT':
        if 'filter_to_delete' in request.form:
            # delete filter by its option name
            res = Equipment.delete_filter(request.form)
        elif 'value' in request.form:
            # add filter for required component
            res = Equipment.add_filter(request.form)
        elif 'operand' in request.form:
            # edit filter
            res = Equipment.edit_requirement_from_form(request.form)
        elif 'max' in request.form:
            # set max count of required component
            res = Equipment.update_max_from_form(request.form)
        elif 'component' in request.form:
            # add new requirement
            res = Equipment.add_requirement_from_form(request.form)
        else:
            # update header
            res = Equipment.update_from_form(request.form)
        if res == 0:
            flash('Произошла ошибка. Попробуйте перезагрузить страницу', 'danger')
    elif request.method == 'DELETE':
        if 'requirement' in request.form:
            res = Equipment.delete_requirement(request.form)
        elif 'component' in request.form:
            res = Equipment.delete_filter(request.form)
        else:
            res = Equipment.delete(id)
        if res == 0:
            flash('Произошла ошибка. Попробуйте перезагрузить страницу', 'danger')
    return jsonify(success=res)

@app.route('/equipment_category', methods=['POST'])
def equipment_category():
    if 'role' not in session or session['role'] != 'employee':
        return redirect('/')
    res = Equipment.add_category(request.form['category'])
    if res == 0:
        flash('Произошла ошибка. Попробуйте перезагрузить страницу', 'danger')
    return redirect('/equipment_manager')


@app.route('/equipment_category/<string:category>', methods=['DELETE', 'PUT'])
def equipment_category_arg(category):
    if 'role' not in session or session['role'] != 'employee':
        return redirect('/')
    if request.method == 'DELETE':
        res = Equipment.delete_category(category)
    else:
        res = Equipment.edit_category_from_form(request.form)
    return jsonify(success=res)


@app.route('/component_manager', methods=['GET'])
def component_manager_redirect():
    if 'role' not in session or session['role'] != 'employee':
        return redirect('/')
    return redirect(url_for('component_manager', category=Components.get_first_category()))


@app.route('/component_manager/<string:category>', methods=['GET'])
def component_manager(category):
    if 'role' not in session or session['role'] != 'employee':
        return redirect('/')
    component_categories = Components.get_categories()
    return render_template('component_manager.html',
        selected_category = category,
        MAX_COMPONENT_OPTIONS_COUNT = settings.MAX_COMPONENT_OPTIONS_COUNT,
        component_warning = Components.warning,
        component_categories = component_categories,
        category_options = Components.get_options(category),
        components = Components.query.filter_by(category=category).all())


@app.route('/component_category', methods=['POST'])
def component_category():
    if 'role' not in session or session['role'] != 'employee':
        return redirect('/')
    res = Components.add_category(request.form)
    if res == 1:
        flash('Категория добавлена', 'success')
        return redirect(url_for('component_manager', category=request.form['category']))
    else:
        flash('Произошла ошибка. Попробуйте перезагрузить страницу', 'danger')
    return redirect('/component_manager')

@app.route('/component_category/<string:category>', methods=["DELETE", "PUT"])
def component_category_arg(category):
    if 'role' not in session or session['role'] != 'employee':
        return redirect('/')
    if request.method == 'DELETE':
        res = Components.delete_category(category)
        if res == -1:
            flash('Невозожно удалить данный тип, так как он используется в оборудовании', 'danger')
    elif request.method == 'PUT':
        if 'changed' in request.form and request.form['changed'] == 'delete':
            # delete component option
            res = Components.delete_option(category, request.form)
        else:
            # edit component option
            res = Components.edit_category_from_form(category, request.form)
    if res == 0:
        flash('Произошла ошибка. Попробуйте перезагрузить страницу', 'danger')
    return jsonify(success=res)


@app.route('/component', methods=["POST"])
def component():
    if 'role' not in session or session['role'] != 'employee':
        return redirect('/')
    res = Components.create_from_form(request.form)
    if res == 0:
        flash('Произошла ошибка. Попробуйте перезагрузить страницу', 'danger')
    else:
        return redirect(url_for('component_manager', category=request.form['category']))
    return redirect('/component_manager')


@app.route('/component/<int:component_id>', methods=["PUT", "DELETE"])
def component_arg(component_id):
    if 'role' not in session or session['role'] != 'employee':
        return redirect('/')
    if request.method == 'PUT':
        if 'option' in request.form:
            res = Components.update_option_from_form(request.form)
        else:
            res = Components.update_from_form(request.form)
    elif request.method == 'DELETE':
        res = Components.delete(component_id)
        if res == 0:
            flash('Произошла ошибка. Попробуйте перезагрузить страницу', 'danger')
        elif res == -1:
            flash('Невозможно удалить данный компонент, так как он задействован в решениях',
                    'danger')
    return jsonify(success=res)


@app.route('/user_manager', methods=['GET'])
def user_manager():
    if 'role' not in session or session['role'] != 'employee':
        return redirect('/')
    return render_template('/user_manager.html',
        users = Users.query.order_by(Users.company).order_by(Users.verified.desc()).all(),
        my_id = session['user_id'])


@app.route('/order_manager', methods=['GET'])
@app.route('/order_manager/', methods=['GET'])
def order_manager():
    if 'user_id' not in session:
        return redirect('/')
    if 'user_id' in request.args and session['role'] != 'employee':
        return redirect('/order_manager')
    user_id = request.args['user_id'] if 'user_id' in request.args else session['user_id']
    orders = Orders.get_orders(user_id = user_id)
    res = Response(render_template('order_manager.html',
        orders = orders,
        users = Users.get_users_for_select()))
    if 'order' in request.cookies:
        res.delete_cookie('order')
        res.delete_cookie('changes_for')
    if 'changed' in request.cookies:
        res.delete_cookie('changed')
    return res


@app.route('/order/<int:id>', methods=['GET', 'PUT', 'DELETE'])
def order(id):
    # TODO: limited access for partners
    if 'role' not in session:
        return redirect('/')
    if request.method == 'GET':
        order = Orders.query.filter_by(order_id=id).first()
        named_data = Orders.get_data(order.data)
        if order is None:
            abort(404, 'Данный заказ не существует.')
        if order.user_id != session['user_id'] and session['role'] != 'employee':
            flash("Вы не являетесь автором данного решения", 'danger')
            return redirect('/order_manager')
        kwargs = {
            'int': int,
            'order': order,
            'data': named_data,
            'str_order': str(order),
            'new_str_order': Orders.get_new_str_order(order),
            'vat': settings.VAT
            }
        if 'changes_for' not in request.cookies:
            res = Response(render_template('order.html', **kwargs))
            res.set_cookie('changes_for', str(order.order_id))
            res.set_cookie('order', urllib.parse.quote(str(order.data)))
            return res
        elif order.order_id != int(request.cookies['changes_for']):
            kwargs['editing_another'] = True
            kwargs['another_order_id'] = int(request.cookies['changes_for'])
            kwargs['another_str_order'] = Orders.__str__(request.cookies['changes_for'])
        else:
            kwargs['data'] = Orders.get_data(eval(urllib.parse.unquote(request.cookies['order'])))
        if session['role'] == 'employee':
            kwargs['users'] = Users.get_users_for_select()
            if session['user_id'] != order.user_id:
                kwargs['save_to_me_number'] = '#' + str(Orders.query.order_by(Orders.order_id.desc()).first().order_id + 1) + '.0'
        return render_template('order.html', **kwargs)
    elif request.method =='PUT':
        if 'save_as' in request.form:
            res = Orders.save(request.form, request.cookies)
            if res == 1:
                flash('Решение успешно сохранено', 'success')
            else:
                flash('Произошла ошибка. Попробуйте перезагрузить страницу', 'danger')
    elif request.method == 'DELETE':
        res = Orders.delete(id)
        if res == 0:
            flash('Произошла ошибка. Попробуйте перезагрузить страницу', 'danger')
    return jsonify(success=res)


@app.route('/order', methods=['POST'])
def create_order():
    if 'user_id' not in session:
        return redirect('/')
    res = Orders.create_from_form(request.form)
    if res == 0:
        # database error
        flash('Произошла ошибка. Попробуйте перезагрузить страницу', 'danger')
        return redirect('/order_manager')
    else:
        return redirect(url_for('order', id=res))


@app.route('/order/<int:id>/export/<string:format>')
def export_order(id, format):
    if 'user_id' not in session:
        return redirect('/')
    file = export(id, format)
    return send_file(file.name, attachment_filename='Sharx решение #' + Orders.__str__(id) + format, as_attachment=True)


@app.route('/update_order/<int:id>', methods=["POST"])
def update_order(id):
    if 'user_id' not in session:
        return redirect('/')
    if Orders.query.filter_by(order_id=id) is None:
        return abort(404, "Данного решения не существует")
    res = Orders.edit_from_form(request.form)
    if res == 0:
        # database error
        flash('Ошибка базы данных', 'danger')
    return redirect(url_for('order', id=id))


@app.route('/order/<int:id>/add_equipment', methods=['GET'])
def add_to_order(id):
    if 'user_id' not in session:
        return redirect('/')
    if 'changes_for' not in request.cookies or int(request.cookies['changes_for']) != id:
        return redirect(url_for('order', id=id))
    order = Orders.query.filter_by(order_id=id).first()
    if order is None:
        return abort(404, "Данного решения не существует")
    return render_template('add_order_equipment.html',
        equipment = Equipment.serialize(),
        order_id = id,
        str_order=str(order),
        categories=Equipment.get_categories())


@app.route('/order/<int:order_id>/<int:equipment_id>', methods=['GET', 'PUT', 'DELETE'])
def order_equipment(order_id, equipment_id):
    if 'user_id' not in session:
        return redirect('/')
    if 'changes_for' not in request.cookies or int(request.cookies['changes_for']) != order_id:
        return redirect(url_for('order', id=order_id))
    category = Equipment.get_category(equipment_id)
    order_cookie = eval(urllib.parse.unquote(request.cookies['order']))
    if category not in order_cookie or str(equipment_id) not in order_cookie[category]:
        flash('Данное оборудование не входит в решение', 'danger')
        return redirect(url_for('order', id=order_id))
    if request.method == 'DELETE':
        res = Orders.delete_equipment(order_id, equipment_id)
        return jsonify(success=res)
    elif request.method == 'GET':
        # render equipment configuration page
        obj = Equipment.query.filter_by(equipment_id=equipment_id).first()
        order = Orders.query.filter_by(order_id=order_id).first()
        if order is None:
            return abort(404, "Данного решения не существует")
        return render_template('configure.html',
            order_id = order_id,
            equipment = obj,
            get_acceptable = Components.get_acceptable,
            vat = settings.VAT
            )
    elif request.method == 'PUT':
        # update configuration
        res = Orders.configure_equipment(order_id, equipment_id, request.form)
        if res == 0:
            flash('Произошла ошибка. Попробуйте перезагрузить страницу', 'danger')
        return jsonify(success=res)


@app.route('/components/<string:category>', methods=["GET"])
def get_components_by_category(category):
    res = {'components': Components.query.filter_by(category=category).all()}
    return Response(jsonpickle(res), mimetype='application/json')


@app.route('/settings', methods=["GET", "POST"])
def settings_manager():
    if 'user_id' not in session or session['role'] != 'employee':
        return redirect('/')
    if request.method == 'GET':
        return render_template('settings.html', settings=settings)
    elif request.method == 'POST':
        res = settings.set_from_form(request.form)
        if res == 0:
            flash('Произошла ошибка. Попробуйте перезагрузить страницу', 'danger')
        else:
            flash('Настройки успешно сохранены', 'success')
        return redirect('/settings')

@app.route('/verify', methods=['GET'])
def verify():
    email = request.args.get('email')
    token = request.args.get('token')
    if Tokens.check(email, token):
        flash('Email успешно подтвержён', 'success')
    else:
        flash('Неверный токен для подтверждения email', 'danger')
    return redirect('/')


@app.route('/register', methods=["POST"])
def register():
    if 'user_id' in session:
        return redirect('/')
    res = Users.create_from_form(request.form)
    if res == -1:
        # Incorrect password action
        flash("Пароли не совпадают", 'danger')
    elif res == 0:
        # Database error action
        flash("Произошла ошибка. Попробуйте перезагрузить страницу", 'danger')
    else:
        # res contains user id now
        # send verification email
        email = request.form['email']
        res = send_verification_link(email)
        if res:
            flash("Подтвердите свой email", 'info')
        else:
            flash("Произошла ошибка при отправке токена для подтверждения email", 'danger')
    return redirect('/login')


@app.route('/login', methods=["GET", "POST"])
def login():
    if 'user_id' in session:
        return redirect('/')
    if request.method == 'GET':
        return render_template('login.html')
    elif request.method == 'POST':
        form = request.form
        username, password = form['username'], form['password']
        id = Users.check_user(username, password)
        if id == -1:
            # if error while logging in
            flash('Неверное имя пользователя или пароль', 'danger')
        elif id == 0:
            # if account is not activated
            flash('Ваш аккаунт не активирован сотрудником компании', 'danger')
        else:
            session.permanent = True  # start session timeout count
            session['user_id'] = id
            session['role'] = Users.get_role(id)
            session['username'] = Users.get_username(id)
            return redirect('/')
        return render_template('login.html')


@app.route('/logout', methods=['GET'])
def logout():
    session.clear()
    res = Response(url_for('login', _external=True), status=302)
    headers = res.headers
    headers['Location'] = url_for('login', _external=True)
    res.headers = headers
    if 'order' in request.cookies:
        res.delete_cookie('order')
    if 'changes_for' in request.cookies:
        res.delete_cookie('changes_for')
    if 'changed' in request.cookies:
        res.delete_cookie('changed')
    return res


@app.route('/edit_user/<int:id>', methods=['GET', 'POST'])
def edit_user(id):
    if 'user_id' not in session:
        return redirect('/')
    if session['role'] == 'partner' and session['user_id'] != id:
        return redirect('/')
    if Users.query.filter_by(user_id=id).first() is None:
        return abort(404, "Данный пользователь не существует")
    if request.method == 'GET':
        return render_template('edit_user.html', user=Users.query.filter_by(user_id=id).first())
    elif request.method == 'POST':
        res = Users.edit_from_form(request.form)
        if res == 0:
            # database error
            flash('Ошибка базы данных', 'danger')
        elif res == -1:
            flash('Старый пароль введён неверно или новые пароли не совпадают', 'danger')
        else:
            flash("Учётные данные успешно изменены", 'success')
        return redirect('/')


@app.route('/block_user/<int:id>', methods=['GET'])
def block_user(id):
    """ change active state """
    if 'role' not in session or session['role'] != 'employee':
        return redirect('/')
    Users.block_user(id)
    return redirect('/user_manager')


@app.route('/delete_user/<int:id>', methods=['GET'])
def delete_user(id):
    if 'user_id' not in session:
        return redirect('/')
    if session['role'] == 'partner' and session['user_id'] != id:
        return redirect('/')
    Users.delete_user(id)
    if session['role'] == 'partner':
        return redirect('/')
    else:
        return redirect('/user_manager')


@app.route('/resend_token/<string:email>', methods=['GET'])
def resend_token(email):
    # only for resend if error occurres
    if 'role' not in session or session['role'] != 'employee':
        return redirect('/')
    try:
        send_verification_link(email)
        flash('Token sent successfully', 'success')
    except Exception as e:
        app.logger.error(e, exc_info=True)
        flash("Error while sending new token", 'danger')
    return redirect('/user_manager')


@app.route('/favicon.ico', methods=["GET"])
def favicon():
    from os.path import join as os_path
    return send_from_directory(os_path(app.root_path, 'static/images'),
                          'favicon.ico' ,mimetype='image/vnd.microsoft.icon')


if __name__ == '__main__':
    schedule_log_archiving()
    app.run(port=config.SERVER_PORT, host=config.SERVER_HOST)
