from setup import *


def test_settings_access(client, init_db_with_users):
    res = login(client, EMPLOYEE)
    assert b'/settings' in res.data


def test_employee_settings_view(client, init_db_with_users):
    login(client, PARTNER)
    res = client.get('/settings', follow_redirects=True)
    assert 'Партнёр' in res.data


def test_employee_settings_view(client, init_db_with_users):
    login(client, EMPLOYEE)
    res = client.get('/settings', follow_redirects=True)
    assert 'Ставка НДС'.encode('utf-8') in res.data
    assert 'Максимальное число свойств'.encode('utf-8') in res.data
    assert 'Текст сообщения о блокировке'.encode('utf-8') in res.data

def test_settings_setting(client, init_db_with_users):
    login(client, EMPLOYEE)
    data = {
        'VAT': '25',
        'MAX_COMPONENT_OPTIONS_COUNT': '115',
        'BLOCKED_USER_EMAIL_TEXT': 'sample text'
    }
    res = client.post('/settings', data=data, follow_redirects=True)
    with open('json/settings.json') as f:
        assert json.load(f) == data

    assert settings.VAT == 25
    assert settings.MAX_COMPONENT_OPTIONS_COUNT == 115
    assert settings.BLOCKED_USER_EMAIL_TEXT == 'sample text'

    assert b'25' in res.data
    assert b'115' in res.data
    assert b'sample text' in res.data
