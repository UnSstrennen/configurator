import pytest
from setup import *


@pytest.fixture(scope='module')
def client():
    app.config['TESTING'] = True
    settings = Settings()
    ctx = app.app_context()
    ctx.push()
    with app.test_client() as client:
        yield client
    ctx.pop()
    os.system("rm -rf logs json")


@pytest.fixture(scope='module')
def init_db():
    db.drop_all()
    db.create_all()
    yield db

@pytest.fixture(scope='module')
def init_db_with_users():
    db.drop_all()
    db.create_all()
    Users.create_from_form(EMPLOYEE)
    Users.create_from_form(PARTNER)
    users = Users.query.all()
    for user in users:
        user.active = True
        user.verified = True
    db.session.add_all(users)
    db.session.commit()
    yield db


@pytest.fixture(scope='module')
def init_db_with_users_components():
    db.drop_all()
    db.create_all()
    Users.create_from_form(EMPLOYEE)
    Users.create_from_form(PARTNER)
    users = Users.query.all()
    for user in users:
        user.active = True
        user.verified = True
    db.session.add_all(users)
    create_json()
    for component in COMPONENTS:
        with open('json/categories.json') as f:
            data = json.load(f)
        if component['category'] not in data['components']:
            out = dict()
            i = 0
            while 'option_' + str(i) in component:
                option = component['option_' + str(i)]
                dimension = component['option_dimension_' + str(i)]
                out[option] = dimension
                i += 1
            data['components'][component['category']] = out
        with open('json/categories.json', mode='w') as f:
            json.dump(data, f)
        res = Components.create_from_form(component)
        if not res:
            raise Exception
        i = 0
        while 'option_' + str(i) in component:
            data = {
                'component_id': Components.query.filter_by(name=component['name']).first().component_id,
                'option': component['option_' + str(i)],
                'value': component['option_value_' + str(i)],
                'dimension': component['option_dimension_' + str(i)]
                }
            res = Components.update_option_from_form(data)
            if not res:
                raise Exception
            i += 1
    db.session.commit()
    yield db


@pytest.fixture(scope='module')
def init_db_with_users_components_equipment():
    db.drop_all()
    db.create_all()
    Users.create_from_form(EMPLOYEE)
    Users.create_from_form(PARTNER)
    users = Users.query.all()
    for user in users:
        user.active = True
        user.verified = True
    db.session.add_all(users)
    create_json()
    with open('json/categories.json') as f:
        data = json.load(f)
    for component in COMPONENTS:
        if component['category'] not in data['components']:
            out = dict()
            i = 0
            while 'option_' + str(i) in component:
                option = component['option_' + str(i)]
                dimension = component['option_dimension_' + str(i)]
                out[option] = dimension
                i += 1
            data['components'][component['category']] = out
        with open('json/categories.json', mode='w') as f:
            json.dump(data, f)
        res = Components.create_from_form(component)
        if not res:
            raise Exception
        i = 0
        while 'option_' + str(i) in component:
            to_set = {
                'component_id': Components.query.filter_by(name=component['name']).first().component_id,
                'option': component['option_' + str(i)],
                'value': component['option_value_' + str(i)],
                'dimension': component['option_dimension_' + str(i)]
                }
            res = Components.update_option_from_form(to_set)
            if not res:
                raise Exception
            i += 1
    for eq in EQUIPMENT:
        if eq['category'] not in data['equipment']:
            data['equipment'].append(eq['category'])
        Equipment.create_from_form(eq)
        obj = Equipment.query.filter_by(category=eq['category'], name=eq['name']).first()
        req = {'Процессор': {'max': 3, 'filters': []}, 'Оперативная память': {'max': 3, 'filters': [{'component_option': 'Объём', 'dimension': 'МБ', 'operator': 'more_eq', 'value': '512'}, {'component_option': 'Интерфейс', 'dimension': '', 'operator': 'eq', 'value': 'SATA'}]}}
        obj.requirements = req
        flag_modified(obj, 'requirements')
        db.session.add(obj)
    with open('json/categories.json', mode='w') as f:
        json.dump(data, f)
    db.session.commit()
    yield db

@pytest.fixture(scope='module')
def init_full_db_with_partner_order():
    db.drop_all()
    db.create_all()
    Users.create_from_form(EMPLOYEE)
    Users.create_from_form(PARTNER)
    users = Users.query.all()
    for user in users:
        user.active = True
        user.verified = True
    db.session.add_all(users)
    create_json()
    with open('json/categories.json') as f:
        data = json.load(f)
    for component in COMPONENTS:
        if component['category'] not in data['components']:
            out = dict()
            i = 0
            while 'option_' + str(i) in component:
                option = component['option_' + str(i)]
                dimension = component['option_dimension_' + str(i)]
                out[option] = dimension
                i += 1
            data['components'][component['category']] = out
        with open('json/categories.json', mode='w') as f:
            json.dump(data, f)
        res = Components.create_from_form(component)
        if not res:
            raise Exception
        i = 0
        while 'option_' + str(i) in component:
            to_set = {
                'component_id': Components.query.filter_by(name=component['name']).first().component_id,
                'option': component['option_' + str(i)],
                'value': component['option_value_' + str(i)],
                'dimension': component['option_dimension_' + str(i)]
                }
            res = Components.update_option_from_form(to_set)
            if not res:
                raise Exception
            i += 1
    for eq in EQUIPMENT:
        if eq['category'] not in data['equipment']:
            data['equipment'].append(eq['category'])
        Equipment.create_from_form(eq)
        obj = Equipment.query.filter_by(category=eq['category'], name=eq['name']).first()
        req = {'Процессор': {'max': 3, 'filters': []}, 'Оперативная память': {'max': 3, 'filters': [{'component_option': 'Объём', 'dimension': 'МБ', 'operator': 'more_eq', 'value': '512'}, {'component_option': 'Интерфейс', 'dimension': '', 'operator': 'eq', 'value': 'SATA'}]}}
        obj.requirements = req
        flag_modified(obj, 'requirements')
        db.session.add(obj)
    with open('json/categories.json', mode='w') as f:
        json.dump(data, f)
    order = Orders(order_name=ORDER_PARTNER['order_name'],
                 customer_name=ORDER_PARTNER['customer_name'],
                 contact_name=ORDER_PARTNER['contact_name'],
                 contact_phone=ORDER_PARTNER['contact_phone'],
                 contact_email=ORDER_PARTNER['contact_email'],
                 description=ORDER_PARTNER['description'],
                 user_id=2,
                 author=ORDER_PARTNER['author'],
                 base_order_id = None,
                 data={'СХД': {'1': {'count': 1, 'id': '1', 'components': {'Оперативная память': 4}},
                 '2': {'count': 1, 'id': '2', 'components': {'Оперативная память': 4}}},
                 'Вычислительный узел': {'3': {'count': 1, 'id': '3', 'components': {'Оперативная память': 4}}}},
                 previous_orders='',
                 created_on=datetime.date(datetime.now()),
                 last_modified=datetime.date(datetime.now()))
    db.session.add(order)
    db.session.commit()
    yield db
