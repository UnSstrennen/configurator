from setup import *


def test_partner_components_access(client, init_db_with_users):
    res = login(client, PARTNER)
    assert b'/equipment_manager' not in res.data
    res = client.get('/equipment_manager', follow_redirects=True)
    assert res.status_code == 200
    assert 'Партнёр'.encode('utf-8') in res.data

def test_employee_components_access(client, init_db_with_users):
    logout(client)
    res = login(client, EMPLOYEE)
    assert b'/equipment_manager' in res.data
    res = client.get('/equipment_manager', follow_redirects=True)
    assert res.status_code == 200
    assert 'Сотрудник'.encode('utf-8') not in res.data

def test_components_view(client, init_db_with_users):
    res = client.get('/component_manager', follow_redirects=True)
    assert 'Добавить тип компонента'.encode('utf-8') in res.data

def test_add_components_categories(client, init_db_with_users):
    res = client.get('/component_manager', follow_redirects=True)
    assert b'/component_category' in res.data
    for component in COMPONENTS:
        with open('json/categories.json') as f:
            data = json.load(f)
            if component['category'] in data['components']:
                continue
        res = client.post('/component_category', data=component, follow_redirects=True)
        assert res.status_code == 200
        assert component['category'].encode('utf-8') in res.data
        with open('json/categories.json') as f:
            data = json.load(f)
            assert component['category'] in data['components']
            if 'option_0' in component:
                assert data['components'][component['category']][component['option_0']] == component['option_dimension_0']

def test_add_components(client, init_db_with_users):
    res = client.get('/component_manager', follow_redirects=True)
    assert 'Добавить тип компонента'.encode('utf-8') in res.data
    assert 'Удалить данный тип'.encode('utf-8') in res.data
    assert 'Изменить свойства типа'.encode('utf-8') in res.data
    for component in COMPONENTS:
        res = client.post('/component', data=component, follow_redirects=True)
        assert res.status_code == 200
        assert component['name'].encode('utf-8') in res.data
        assert component['part_number'].encode('utf-8') in res.data
        assert component['category'].encode('utf-8') in res.data
        assert Components.query.filter_by(category=component['category'], name=component['name']).first() is not None
    assert 'Удалить компонент'.encode('utf-8') in res.data


def test_set_components_options(client, init_db_with_users):
    for component in COMPONENTS:
        i = 0
        while 'option_' + str(i) in component:
            id = Components.query.filter_by(name=component['name'],
            category=component['category']).first().component_id
            data = {
                'component_id': id,
                'option': component['option_' + str(i)],
                'value': component['option_value_' + str(i)],
                'dimension': component['option_dimension_' + str(i)]
            }
            res = client.put('/component/' + str(id), data=data, follow_redirects=True)
            assert res.status_code == 200
            obj = Components.query.filter_by(component_id=id).first()
            assert obj is not None
            meta = obj.meta
            assert meta[data['option']] == {'dimension': data['dimension'], 'value': data['value']}
            assert res.get_json() == {'success': 1}
            res = client.get('/component_manager', follow_redirects=True)
            assert res.status_code == 200
            i += 1
