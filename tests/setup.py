import os, sys
from copy import deepcopy
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import pytest

from server import *
from setter import *


def login(client, data):
    res = client.post('/login', data=data, follow_redirects=True)  # login as employee
    assert session
    return res


def logout(client):
    res = client.get('/logout', follow_redirects=True)
    assert not session
    return res


EMPLOYEE = {
    'username': 'test_employee',
    'name': 'Test',
    'surname': 'Testovich',
    'company': 'Testers LLC',
    'email': 'nshishkin@sharxdc.ru',
    'phone': '+79255633696',
    'password': '123456789',
    'repeat_password': '123456789'
}

PARTNER = {
    'username': 'test_partner',
    'name': 'Partner',
    'surname': 'Partnerov',
    'company': 'Partnerd LTD',
    'email': 'freelancer-nikita@yandex.ru',
    'phone': '+79255633696',
    'password': '123456789',
    'repeat_password': '123456789'
}

EQUIPMENT = [
    {
        'category': 'СХД',
        'name': 'СХД1',
        'part_number': 'SHD1'
    },
    {
        'category': 'СХД',
        'name': 'СХД2',
        'part_number': 'SHD2'
    },
    {
        'category': 'Вычислительный узел',
        'name': 'ВычУз',
        'part_number': 'VU123'
    }
]

COMPONENTS = [
    {
        'category': 'Процессор',
        'name': 'Процессор1',
        'part_number': 'PROC1',
        'cost': '115.25',
        'price': '180',
        'vat': 'vat',
        'description': 'test description 1101',
        'option_0': 'Количество ядер',
        'option_value_0': '16',
        'option_dimension_0': 'ядер',
    },
    {
        'category': 'Оперативная память',
        'name': 'ОперативнаяПамять1',
        'part_number': 'OPMEM1',
        'cost': '800',
        'price': '1000',
        'description': 'test description оперативная память',
        'option_0': 'Объём',
        'option_value_0': '1024',
        'option_dimension_0': 'МБ',
        'option_1': 'Интерфейс',
        'option_value_1': 'SATA2',
        'option_dimension_1': ''
    },
    {
        'category': 'Процессор',
        'name': 'Процессор2',
        'part_number': 'PROC2',
        'cost': '120.50',
        'price': '200.5',
        'description': '',
        'option': 'Количество ядер',
        'option_value_0': '16',
        'option_dimension_0': 'ядер',
    },
    {
        'category': 'Оперативная память',
        'name': 'ОперативнаяПамять2',
        'part_number': 'OPMEM2',
        'cost': '1',
        'price': '1',
        'vat': 'vat',
        'description': '',
        'option_0': 'Объём',
        'option_value_0': '512',
        'option_dimension_0': 'МБ',
        'option_1': 'Интерфейс',
        'option_value_1': 'SATA',
        'option_dimension_1': ''
    }
]

ORDER_PARTNER = {
    'order_name': 'test_solution',
    'author': PARTNER['username'],
    'customer_name': 'Partners Inc.',
    'contact_name': 'Partner Partnerov',
    'contact_phone': '880005553535',
    'contact_email': 'partner@partner.com',
    'description': 'test description'
}

ORDER_EMPLOYEE = {
    'order_name': 'test_employee_solution',
    'author': EMPLOYEE['username'],
    'customer_name': 'SharxDC LLC',
    'contact_name': 'Akul Akulov',
    'contact_phone': '880005553535',
    'contact_email': 'test@sharxdc.ru',
    'description': ''
}
