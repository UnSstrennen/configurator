from setup import *


def test_access_to_partner_orders(client, init_full_db_with_partner_order):
    assert Orders.query.first() is not None
    login(client, EMPLOYEE)
    res = client.get('/order_manager', follow_redirects=True)
    assert res.status_code == 200
    assert PARTNER['name'].encode('utf-8') in res.data
    partner_id = Users.query.filter_by(username=PARTNER['username']).first().user_id
    res = client.get('/order_manager/?user_id=' + str(partner_id))
    assert res.status_code == 200
    assert b'1.0' in res.data
    res = client.get('/order/1')
    assert res.status_code == 200
    assert b'#1.0' in res.data
    assert 'к себе'.encode('utf-8') in res.data
    data = {
        'order_id': 1,
        'save_as': 'as_another_user',
        'user_id': 1
    }
    res = client.put('/order/1', data=data, follow_redirects=True)
    assert res.status_code == 200
    assert Orders.query.filter_by(user_id=1).first() is not None
    id = Orders.query.filter_by(user_id=1).first().order_id
    res = client.get('/order_manager', follow_redirects=True)
    assert res.status_code == 200
    assert str(str(id) + '.0').encode('utf-8') in res.data

def test_copy_editing_and_saving(client, init_full_db_with_partner_order):
    res = client.get('/order/2', follow_redirects=True)
    res = client.get('/order/2', follow_redirects=True)
    assert res.status_code == 200
    assert b'#2.0' in res.data
    prev_order = eval(urllib.parse.unquote(request.cookies['order']))
    obj = Orders.query.filter_by(order_id=1).first()
    assert obj is not None
    assert obj.data == prev_order
    # make changes manually
    modified_order = {'СХД': {'1': {'count': 1, 'id': '1', 'components': {'Оперативная память': 4}},
    '2': {'count': 1, 'id': '2', 'components': {'Оперативная память': 4}}}}
    client.set_cookie('/', 'order', json.dumps(modified_order))
    res = client.get('/order/2')
    assert modified_order == eval(urllib.parse.unquote(request.cookies['order']))
    data = {
        'order_id': 2,
        'save_as': 'as_current'
    }
    res = client.put('/order/2', data=data, follow_redirects=True)
    order = Orders.query.filter_by(order_id=2).first()
    assert order is not None
    assert order.data == modified_order
    res = client.get('/order_manager', follow_redirects=True)
    assert res.status_code == 200
    assert b'2.0' in res.data
    assert b'2.1' not in res.data


def test_partner_data_is_saved(client, init_full_db_with_partner_order):
    prev_order = {'СХД': {'1': {'count': 1, 'id': '1', 'components': {'Оперативная память': 4}},
    '2': {'count': 1, 'id': '2', 'components': {'Оперативная память': 4}}},
    'Вычислительный узел': {'3': {'count': 1, 'id': '3', 'components': {'Оперативная память': 4}}}}
    order = Orders.query.filter_by(order_id=1).first()
    assert order is not None
    assert order.data == prev_order
    logout(client)
    if 'order' in request.cookies:
        client.delete_cookie('/', 'order')
    if 'changes_for' in request.cookies:
        client.delete_cookie('/', 'changes_for')
    if 'changed' in request.cookies:
        client.delete_cookie('/', 'changed')
    login(client, PARTNER)
    res = client.get('/order_manager', follow_redirects=True)
    assert 'order' not in request.cookies
    assert b'1.0' in res.data
    res = client.get('/order/1', follow_redirects=True)
    # because of the flask problem: cookie operations for user are clear
    res = client.get('/order/1', follow_redirects=True)
    assert 'другого решения'.encode('utf-8') not in res.data
    assert eval(urllib.parse.unquote(request.cookies['order'])) == prev_order

def test_exporting(client, init_full_db_with_partner_order):
    logout(client)
    login(client, EMPLOYEE)
    res = client.get('/order/2', follow_redirects=True)
    assert res.status_code == 200
    assert b'.xls' in res.data
    assert b'.xlsx' in res.data
    res = client.get('/order/2/export/.xlsx')
    assert res.status_code == 200

def test_deleting(client, init_full_db_with_partner_order):
    res = client.get('/order_manager', follow_redirects=True)
    assert res.status_code == 200
    assert b'2.0' in res.data
    assert b'delete_order(2' in res.data
    res = client.delete('/order/2', follow_redirects=True)
    assert res.status_code == 200
    assert b'2.0' not in res.data
    assert Orders.query.filter_by(order_id=2).first() is None
    res = client.get('/order_manager/?user_id=2', follow_redirects=True)
    assert res.status_code == 200
    assert b'1.0' in res.data
    assert b'delete_order(1' not in res.data

def test_creating_employee_order(client, init_full_db_with_partner_order):
    res = client.get('/order_manager', follow_redirects=True)
    assert res.status_code == 200
    res = client.post('/order', data=ORDER_EMPLOYEE, follow_redirects=True)
    assert res.status_code == 200
    obj = Orders.query.filter_by(order_name=ORDER_EMPLOYEE['order_name']).first()
    assert obj is not None
    id = obj.order_id
    assert str(obj).encode('utf-8') in res.data
