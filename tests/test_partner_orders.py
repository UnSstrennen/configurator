from setup import *


def test_creating(client, init_db_with_users_components_equipment):
    login(client, PARTNER)
    res = client.get('/order_manager', follow_redirects=True)
    assert res.status_code == 200
    res = client.post('/order', data=ORDER_PARTNER, follow_redirects=True)
    assert res.status_code == 200
    obj = Orders.query.filter_by(order_name=ORDER_PARTNER['order_name']).first()
    assert obj is not None
    id = obj.order_id
    assert str(obj).encode('utf-8') in res.data

def test_add_equipment(client, init_db_with_users_components_equipment):
    res = client.get('/order/1/add_equipment')
    assert res.status_code == 200
    assert 'Добавить оборудование'.encode('utf-8') in res.data
    for eq in EQUIPMENT:
        assert eq['category'].encode('utf-8') in res.data
        assert eq['name'].encode('utf-8') in res.data
    # manually set cookie
    to_set = dict()
    for eq in EQUIPMENT:
        if eq['category'] not in to_set:
            to_set[eq['category']] = dict()
        obj = Equipment.query.filter_by(name=eq['name']).first()
        assert obj is not None
        id = str(obj.equipment_id)
        to_set[eq['category']][id] = {
            'count': 1,
            'id': id,
            'components': {}
        }
    client.set_cookie('/', 'order', json.dumps(to_set))
    res = client.get('/order/1')
    assert res.status_code == 200
    assert eval(urllib.parse.unquote(request.cookies['order'])) == to_set
    for eq in EQUIPMENT:
        print(eq['name'])
        assert eq['category'].encode('utf-8') in res.data
        assert eq['name'].encode('utf-8') in res.data

def test_configuring(client, init_db_with_users_components_equipment):
    assert 'order' in request.cookies
    cookie = eval(urllib.parse.unquote(request.cookies['order']))
    total_price = 0.0
    for eq in EQUIPMENT:
        obj = Equipment.query.filter_by(name=eq['name']).first()
        assert obj is not None
        id = obj.equipment_id
        # try to add another data
        res = client.get('/order/1/' + str(id))
        assert res.status_code == 200
        assert 'Нет подходящих компонентов для данного требования.'.encode('utf-8') not in res.data
        for comp in COMPONENTS:
            if 'option_value_1' in comp and comp['option_value_1'] != 'SATA2':
                assert comp['name'].encode('utf-8') in res.data
                # set cookie manually
                comp_id = Components.query.filter_by(name=comp['name']).first().component_id
                if comp['category'] in cookie[eq['category']][str(id)]['components']:
                    continue
                cookie[eq['category']][str(id)]['components'][comp['category']] = int(comp_id)
                total_price += float(comp['price'])
        client.set_cookie('/', 'order', json.dumps(cookie))
        res = client.get('/order/1')
        assert res.status_code == 200
        assert eval(urllib.parse.unquote(request.cookies['order'])) == cookie
        assert str(comp['price']).encode('utf-8') in res.data


def test_save(client, init_db_with_users_components_equipment):
    assert 'order' in request.cookies
    cookie = eval(urllib.parse.unquote(request.cookies['order']))
    res = client.get('/order/1')
    assert res.status_code == 200
    assert 'Сохранить'.encode('utf-8') in res.data
    assert 'текущую версию'.encode('utf-8') in res.data
    assert 'новую версию'.encode('utf-8') in res.data
    assert 'другому пользователю'.encode('utf-8') not in res.data
    data = {
        'save_as': 'as_new',
        'order_id': 1
    }
    assert b'#1.1' in res.data
    res = client.put('/order/1', data=data, follow_redirects=True)
    assert res.status_code == 200
    assert 'changes_for' in request.cookies
    assert 'order' in request.cookies
    assert 'changed' not in request.cookies
    order = Orders.query.filter_by(order_id=2).first()
    assert order is not None
    assert order.data == cookie

def test_view(client, init_db_with_users_components_equipment):
    res = client.get('/order_manager', follow_redirects=True)
    assert res.status_code == 200
    assert b'1.0' in res.data
    assert b'1.1' in res.data

def test_exporting(client, init_db_with_users_components_equipment):
    res = client.get('/order/1')
    assert res.status_code == 200
    assert b'.xls' in res.data
    assert b'.xlsx' in res.data
    res = client.get('/order/1/export/.xlsx')
    assert res.status_code == 200

def test_deleting(client, init_db_with_users_components_equipment):
    res = client.get('/order_manager', follow_redirects=True)
    assert res.status_code == 200
    res = client.delete('/order/1', follow_redirects=True)
    assert res.status_code == 200
    assert b'1.0' not in res.data
