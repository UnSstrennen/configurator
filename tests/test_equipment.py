from setup import *


def test_partner_equipment_access(client, init_db_with_users_components):
    res = login(client, PARTNER)
    assert b'/equipment_manager' not in res.data
    res = client.get('/equipment_manager', follow_redirects=True)
    assert res.status_code == 200
    assert 'Партнёр'.encode('utf-8') in res.data

def test_employee_equipment_access(client, init_db_with_users_components):
    logout(client)
    res = login(client, EMPLOYEE)
    assert b'/equipment_manager' in res.data
    res = client.get('/equipment_manager', follow_redirects=True)
    assert res.status_code == 200
    assert 'Сотрудник'.encode('utf-8') not in res.data


def test_equipment_view(client, init_db_with_users_components):
    res = client.get('/equipment_manager', follow_redirects=True)
    assert 'Добавить тип оборудования'.encode('utf-8') in res.data


def test_add_eqipment_categories(client, init_db_with_users_components):
    res = client.get('/equipment_manager', follow_redirects=True)
    assert b'/equipment_category' in res.data
    was = list()
    for equipment in EQUIPMENT:
        with open('json/categories.json') as f:
            data = json.load(f)
        if equipment['category'] in data['equipment']:
            continue
        res = client.post('/equipment_category', data=equipment, follow_redirects=True)
        assert res.status_code == 200
        assert equipment['category'].encode('utf-8') in res.data
        with open('json/categories.json') as f:
            data = json.load(f)
            assert equipment['category'] in data['equipment']

def test_add_equipment(client, init_db_with_users_components):
    assert Components.query.all()
    res = client.get('/equipment_manager', follow_redirects=True)
    assert 'Добавить тип оборудования'.encode('utf-8') in res.data
    assert 'Удалить данный тип'.encode('utf-8') in res.data
    assert 'Изменить название типа'.encode('utf-8') in res.data
    assert 'Удалить данный тип'.encode('utf-8') in res.data
    for equipment in EQUIPMENT:
        res = client.post('/equipment', data=equipment, follow_redirects=True)
        assert res.status_code == 200
        assert equipment['name'].encode('utf-8') in res.data
        assert equipment['part_number'].encode('utf-8') in res.data
        assert equipment['category'].encode('utf-8') in res.data
        assert Equipment.query.filter_by(category=equipment['category'], name=equipment['name']).first() is not None
    assert 'Удалить оборудование'.encode('utf-8') in res.data

def test_set_equipment_requirements(client, init_db_with_users_components):
    for eq in EQUIPMENT:
        id = Equipment.query.filter_by(category=eq['category'], name=eq['name']).first().equipment_id
        was = list()
        for component in COMPONENTS:
            if component['category'] in was:
                continue
            data = {
                'equipment_id': id,
                'component': component['category']
            }
            res = client.put('/equipment/' + str(id), data=data, follow_redirects=True)
            assert res.status_code == 200
            assert res.get_json() == {'success': 1}
            res = client.get('/equipment_manager', follow_redirects=True)
            assert data['component'].encode('utf-8') in res.data
            obj = Equipment.query.filter_by(equipment_id=id).first()
            assert obj is not None
            assert component['category'] in obj.requirements
            data['max'] = 3
            res = client.put('/equipment/' + str(id), data=data, follow_redirects=True)
            assert res.status_code == 200
            obj = Equipment.query.filter_by(equipment_id=id).first()
            assert obj.requirements[component['category']]['max'] == 3
            i = 0
            del data['max']
            while 'option_' + str(i) in component:
                value = component['option_value_' + str(i)]
                data['component_option'] = component['option_' + str(i)]
                data['operator'] = 'more_eq' if type(value) is int or value.isdigit() else 'eq'
                data['value'] = value
                res = client.put('/equipment/' + str(id), data=data)
                assert res.status_code == 200
                assert res.get_json() == {'success': 1}
                i += 1

def test_deleting_components_used_in_equipment(client, init_db_with_users_components):
    res = client.get('/component_manager/Процессор')
    assert res.status_code == 200
    assert 'Процессор'.encode('utf-8') in res.data
    res = client.delete('/component_category/Процессор', follow_redirects=True)
    assert res.status_code == 200
    assert res.get_json() == {'success': -1}
    res = client.get('/component_manager/Процессор')
    assert res.status_code == 200
    assert 'используется в оборудовании'.encode('utf-8') in res.data
