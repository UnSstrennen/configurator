from setup import *


def test_main_page(client, init_db):
    res = client.get('/', follow_redirects=True)
    assert res.status_code == 200


def test_create_employee(client):
    res = client.get('/login')
    assert res.status_code == 200
    assert 'Регистрация'.encode('utf-8') in res.data

    # test if incorrect password
    data = deepcopy(EMPLOYEE)
    data['repeat_password'] = 'asdfgh'
    res = client.post('/register', data=data, follow_redirects=True)
    assert 'Пароли не совпадают'.encode('utf-8') in res.data

    # test if ok
    data['repeat_password'] = data['password']
    res = client.post('/register', data=data, follow_redirects=True)
    assert 'Подтвердите свой email'.encode('utf-8') in res.data
    assert res.status_code == 200
    assert 'Зарегестрироваться'.encode('utf-8') in res.data  # at login page


def test_employee_token_accepting(client, init_db):
    assert Tokens.query.filter_by(email=EMPLOYEE['email']).first() is not None
    res = client.get('/verify?email={}&token={}'.format(EMPLOYEE['email'], 'aaaaa'), follow_redirects=True)
    assert 'Неверный токен для подтверждения email'.encode('utf-8') in res.data
    # check that correct token hasn't been deleted
    assert Tokens.query.filter_by(email=EMPLOYEE['email']).first() is not None
    token = Tokens.query.filter_by(email=EMPLOYEE['email']).first().token
    res = client.get('/verify?email={}&token={}'.format(EMPLOYEE['email'], token), follow_redirects=True)
    assert res.status_code == 200
    assert 'Email успешно подтвержён'.encode('utf-8') in res.data
    assert Users.query.filter_by(username=EMPLOYEE['username']).first().verified
    assert Users.query.filter_by(username=EMPLOYEE['username']).first().active


def test_employee_login(client):
    res = client.get('/login')
    assert res.status_code == 200
    res = client.post('/login', data=EMPLOYEE, follow_redirects = True)
    assert res.status_code == 200
    user = Users.query.filter_by(username=EMPLOYEE['username']).first()
    assert user.active
    assert 'Сотрудник'.encode('utf-8') in res.data
    assert session['username'] == user.username
    assert session['user_id'] == user.user_id
    assert session['role'] == user.role
    assert 'Вы ещё не создали ни одного решения.'.encode('utf-8') in res.data


def test_logout(client):
    assert 'user_id' in session
    res = client.get('/logout', follow_redirects=True)
    assert res.status_code == 200
    assert 'Войти'.encode('utf-8') in res.data
    assert 'user_id' not in session


def test_create_partner(client):

    res = client.get('/login')
    assert 'Регистрация'.encode('utf-8') in res.data
    assert res.status_code == 200

    # test if incorrect password
    data = deepcopy(PARTNER)
    data['repeat_password'] = 'asdfgh'
    res = client.post('/register', data=data, follow_redirects=True)
    assert 'Пароли не совпадают'.encode('utf-8') in res.data

    # test if ok
    data['repeat_password'] = data['password']
    res = client.post('/register', data=data, follow_redirects=True)
    assert 'Подтвердите свой email'.encode('utf-8') in res.data
    assert 'Зарегестрироваться'.encode('utf-8') in res.data  # at login page


def test_partner_token_accepting(client, init_db):
    assert Tokens.query.filter_by(email=PARTNER['email']).first() is not None
    res = client.get('/verify?email={}&token={}'.format(PARTNER['email'], 'aaaaa'), follow_redirects=True)
    assert 'Неверный токен для подтверждения email'.encode('utf-8') in res.data
    # check that correct token hasn't been deleted
    assert Tokens.query.filter_by(email=PARTNER['email']).first() is not None
    token = Tokens.query.filter_by(email=PARTNER['email']).first().token
    res = client.get('/verify?email={}&token={}'.format(PARTNER['email'], token), follow_redirects=True)
    assert res.status_code == 200
    assert 'Email успешно подтвержён'.encode('utf-8') in res.data
    assert Users.query.filter_by(username=PARTNER['username']).first().verified
    assert not Users.query.filter_by(username=PARTNER['username']).first().active


def test_partner_login(client):
    res = client.get('/login')
    assert res.status_code == 200
    res = client.post('/login', data=PARTNER, follow_redirects = True)
    assert res.status_code == 200
    assert 'Ваш аккаунт не активирован сотрудником компании'.encode('utf-8') in res.data
    # activate user manually
    user = Users.query.filter_by(username=PARTNER['username']).first()
    assert user is not None
    assert not user.active
    user.active = True
    db.session.add(user)
    db.session.commit()
    user = Users.query.filter_by(username=PARTNER['username']).first()
    assert user.active
    res = client.post('/login', data=PARTNER, follow_redirects = True)
    assert 'Партнёр'.encode('utf-8') in res.data
    assert session['username'] == user.username
    assert session['user_id'] == user.user_id
    assert session['role'] == user.role
