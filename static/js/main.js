$( document ).ready(function() {
  /*
  // TODO: remove and change to locatStorage selecting
  $('.nav-pills').each(function () {
    var first = $(this.firstElementChild);
    first.click();
    first.attr('aria-selected', 'true');
  });

  // TODO: remove and change to locatStorage selecting
  $('.tab-content').each(function () {
    var first = $(this.firstElementChild);
    first.addClass('show active');
  }); */

  $('.nav-main').each(function () {
    if (location.href.indexOf(this.href) != -1 ||
      (location.href.indexOf('order') != -1 && this.href.indexOf('order') != -1))
      $(this).addClass('active');
  });
})

$('.nav-tabs').click(function() {
  sessionStorage.chosen_id = this.id.split('-')[2];
});

function isJson(obj) {
  if (obj.constructor instanceof Object) return true;
  try {
    JSON.parse(obj);
    return true;
  }
  catch (e) {
    return false;
  }
}

function form_by_ajax(e, path, callback) {
  var method = $(e).attr('method');
  var url = window.location.protocol + '//' + window.location.host + path;
  $.ajax({
    type: method,
    url: url,
    data: $(e).serialize(),
    dataType: 'json',
    complete: function(data, text_status) {
      if (data.redirect) window.location.href = data.redirect;
      if (isJson(data.responseText))
        var res = JSON.parse(data.responseText)['success'];
     else
        var res = ['success', 'notmodified', 'nocontent'].indexOf(text_status) != -1;
      if (callback !== undefined) callback(res);
    },
  }).fail(function(xhr, ajaxOptions, thrownError) {
    console.log(thrownError);
  });
}
