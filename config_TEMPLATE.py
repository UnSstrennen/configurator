# fill config with your settings, it's just a template
# after filling the data, please rename this file to config.py

# DATABASE
DATABASE_TYPE = ''  # mysql, postgresql, etc.
CONNECTOR = ''
DATABASE_HOST = ''
USERNAME = ''
PASSWORD = ''
DATABASE_PORT = 0
DATABASE = ''

# APP
SESSION_LIFETIME = 0  # in minutes
SECRET_KEY = ''  # for CSRF protection
SERVER_HOST = ''
SERVER_PORT = 0
DEBUG = False  # dev only
LOGGING_BACKUP_PERIOD = 0  # in months
UTC = False  # set to True if server time is UTC instead of local time

# MAIL
EMPLOYEE_MAIL_DOMAIN = ''  # for differ employees emails
SMTP_SERVER = ''
SMTP_PORT = 0
SMTP_USE_SSL = False
SMTP_USE_TLS = False
MAILER_EMAIL = ''  # email which will be used to send auto-emails
MAILER_PASSWORD = ''
