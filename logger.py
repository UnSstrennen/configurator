from apscheduler.schedulers.background import BackgroundScheduler
import atexit
from datetime import datetime
from pytz import utc
from shutil import copyfile
from os.path import exists

import config


scheduler = BackgroundScheduler()


def archive_log(dt_obj=datetime.now()):
    copyfile('logs/log.log', dt_obj.strftime('logs/log_%b_%Y.log'))
    open('logs/log.log', 'w').close()


def check_if_new():
    if not exists('logs/log.log'):
        return
    last_line = str()
    with open('logs/log.log', 'r') as f:
        for line in f.readlines()[::-1]:
            if '[' in line and ']' in line and config.SERVER_HOST in line:
                last_line = line
                break
    if not last_line:
        return
    log_last_date = last_line[last_line.index('[') + 1:last_line.index(']')].split()[0]
    log_month = datetime.strptime(log_last_date, '%d/%b/%Y').month
    if log_month != datetime.now().month:
        archive_log(datetime.strptime(log_last_date, '%d/%b/%Y'))


def schedule_log_archiving():
    check_if_new()
    scheduler.add_job(archive_log, trigger="cron", day=1)
    if config.UTC:
        scheduler.configure(timezone=utc)
    scheduler.start()
    atexit.register(lambda: scheduler.shutdown())
