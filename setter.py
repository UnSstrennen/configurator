import json
from os.path import exists
from os import makedirs


class Settings:
    path = 'json/settings.json'

    def __init__(self):
        """ creates category.json if not exists """
        if not exists(self.path):
            if not exists('json'):
                makedirs('json')
            with open(self.path, mode='w') as f:
                json.dump({'VAT': 20, 'MAX_COMPONENT_OPTIONS_COUNT': 5,
                'BLOCKED_USER_EMAIL_TEXT':
                'Здравствуйте, вас заблокировали, всего хорошего'}, f)
        else:
            with open(self.path) as f:
                if len(f.read()) == 0:
                    json.dump({'VAT': 20, 'MAX_COMPONENT_OPTIONS_COUNT': 5,
                    'BLOCKED_USER_EMAIL_TEXT':
                    'Здравствуйте, вас заблокировали, всего хорошего'}, f)

    def __dict__(self):
        with open(self.path) as f:
            res = json.load(f)
        return res

    def set_from_form(self, form):
        try:
            self.VAT = form['VAT']
            self.MAX_COMPONENT_OPTIONS_COUNT = form['MAX_COMPONENT_OPTIONS_COUNT']
            self.BLOCKED_USER_EMAIL_TEXT = form['BLOCKED_USER_EMAIL_TEXT']
            return 1
        except Exception:
            return 0

    def __getattr__(self, name):
        with open(self.path) as f:
            res = json.load(f)[name]
            if type(res) is str and res.isdigit():
                res = int(res)
        return res

    def __setattr__(self, name, value):
        with open(self.path) as f:
            res = json.load(f)
        if type(value) is int and value.isdigit():
            value = int(value)
        res[name] = value
        with open(self.path, mode="w") as f:
            res = json.dump(res, f)

settings = Settings()
